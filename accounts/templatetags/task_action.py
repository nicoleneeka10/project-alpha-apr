from datetime import datetime
from django import template
register = template.Library()


@register.simple_tag
def getMostUrgentTaskForUser(user):
    allProjects = user.projects.all()
    closest_due_date = None
    mostUrgentTask = None
    time_now = datetime.today().replace(tzinfo=None)
    for project in allProjects:
        for task in project.tasks.filter(is_completed=False, assignee=user.id):
            days_away_from_today = time_now - task.due_date.replace(tzinfo=None)
            if closest_due_date is None:
                closest_due_date = days_away_from_today
                mostUrgentTask = task
            elif days_away_from_today > closest_due_date:
                closest_due_date = days_away_from_today
                mostUrgentTask = task
    if mostUrgentTask is None:
        return None
    taskNameToDisplay = mostUrgentTask.name
    projectIdOfTheTaskYouAreDIsplaying = mostUrgentTask.project.id
    test = {
        "projectId": projectIdOfTheTaskYouAreDIsplaying,
        "taskName": taskNameToDisplay}
    return test

# mostUrgentTask = user.projects.all()[0].tasks.all()[0]



def chart_month_num_start_end(task):
    start_month_plus = task.start_date.month + 1
    end_month_plus = task.due_date.month + 2
    month_nums = {
        "chart_month_start": start_month_plus,
        "chart_month_end": end_month_plus}
    return month_nums


# @register.simple_tag
# def check_task_year(tasks):
#     this_yr_tasks = []
#     for task in tasks:
#         task_start_yr = task.start_date.year
#         task_end_yr = task.due_date.year
#         year_now = datetime.today().year
#         if task_start_yr == year_now or task_end_yr == year_now:
#             this_yr_tasks.append(task)
#     return this_yr_tasks

@register.inclusion_tag('projects/projectdetailganttchart.html')
def check_task_year_list_view(tasks):
    this_yr_task_info = {}
    for task in tasks:
        task_start_yr = task.start_date.year
        task_end_yr = task.due_date.year
        year_now = datetime.today().year
        if task_start_yr == year_now or task_end_yr == year_now:
            numbers = chart_month_num_start_end(task)
            color = chartbarcolor(task)
            bar_edges = bar_round_or_curved(task)
            this_yr_task_info[task] = {
                "numbers": numbers,
                "color": color,
                "edges": bar_edges
            }
    context = {
        "this_yr_task_info": this_yr_task_info,
    }
    return context


def chartbarcolor(task):
    time_now = datetime.today().replace(tzinfo=None)
    if task.is_completed is True:
        return "greenbar"
    elif time_now > task.due_date.replace(tzinfo=None):
        return "redbar"
    else:
        return "yellowbar"


def bar_round_or_curved(task):
    task_start_yr = task.start_date.year
    task_end_yr = task.due_date.year
    year_now = datetime.today().year
    return_str = ""
    if task_end_yr == year_now:
        return_str = return_str + "roundright "
    if task_start_yr == year_now:
        return_str = return_str + "roundleft"
    return return_str



    # if task_end_yr == year_now and task_start_yr == year_now:
    #     return "roundright roundleft"
    # elif task_start_yr != year_now and task_end_yr == year_now:
    #     return "roundright"
    # elif task_start_yr == year_now and task_end_yr != year_now:
    #     return "roundleft"
    # else:
    #     return ""
